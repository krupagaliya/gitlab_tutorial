from time import sleep


def first_Call():
    print("Hello1")

def seconf_Call():
    print("Hello2")

def third_Call():
    print("Hello3")

def api():
    first_Call()
    # yield
    seconf_Call()
    # yield
    third_Call()
    # yield

api()
print("Now you can do it ")